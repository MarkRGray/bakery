using bakery.BakedGoods;
using Moq;
using NUnit.Framework;
using FluentAssertions;
using bakery.CookingEquipment;
using bakery.Recipes;

namespace bakery.tests
{
    public class OvenShould
    {
        [Test]
        public void BakeTheBread()
        {
            var ingredientWeigher = new Mock<IWeigher>();
            var kneader = new Mock<IKneader>();
            var prover = new Mock<IProver>();
            var oven = new Oven(ingredientWeigher.Object, kneader.Object, prover.Object);
            var recipe = new Recipe();
            var bakedGood = oven.Bake(recipe);
            bakedGood.Type.Should().Be(BakedGoodType.Bread);
        }
    }
}